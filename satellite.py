# author@zhangcsh  SYSU, TianQin Research Center
# mail: zhangcsh5@gmail.com
# date: 2022-05-06

import os
import sys
import shutil
import numpy as np
import pandas as pd
from ctypes import *
from numpy.lib import loadtxt, polyfit
from datetime import datetime as dt

# speed of light in m/s
CMPS = 299792458.0
respath = os.getcwd()
if sys.platform.startswith('linux'):
    # Linux
    liblagrange = cdll.LoadLibrary(respath + '/lagrange.x86_64-linux-gnu.so')
    libres = cdll.LoadLibrary(respath + '/getres.so')
elif sys.platform.startswith('win'):
    # Windows
    liblagrange = cdll.LoadLibrary(respath + '/lagrange.win_amd64-msvc10.dll')
    libres = cdll.LoadLibrary(respath + '/getres.dll')
else:
    raise Exception('Unsupported platform')
libres.getres.argtypes=(c_char_p,c_char_p,c_char_p)

def lagrange(xp, nval, c_x, y):
    """
    Return interpolated value of y at xp by lagrange formula.

    Parameters
    ----------
    xp : double
        interpolated value.

    nval : int
        Points to be used.

    c_x : (N,) array_like of c_double
        A 1-D array of real values.

    y : (...,N)
        A N-D array of real values. The length of `y` along the interpolation
        axis must be equal to the length of `x`.

    Returns
    -------
        Interpolated value.
    """
    # prepare var
    count = len(c_x)

    if type(c_x) is list:
        # if c_x is not a array_like of c_double but a list, convert it.
        x = c_x
        c_array_x = c_double * count
        c_x = c_array_x()
        for i in range(0, count):
            c_x[i] = x[i]
    #print(c_x)
    c_array_type = c_double * nval
    c_funcs = c_array_type()
    c_start_index = c_int(0)

    # calculate lagrange basic functions

    ret = liblagrange.lagrange_funcs(
            c_double(xp), nval, count, byref(c_x),
            byref(c_funcs), byref(c_start_index))

    if ret == 0:  # success
        ydim = len(y)                       
        start_index = c_start_index.value

        # calcalute y by lagrange basic functions
        # 2-3d            
        if ydim <= 3: 
            yp = [0] * ydim
            for i in range(0, nval):                           
                for k in np.arange(0, ydim,1):
                    #print(k)                
                    #print([i + start_index])
                    yp[k] += y[k][i + start_index] * c_funcs[i]
        # one dimension
        else:
            yp = [0]
            for i in range(0, nval):
                yp += y[i + start_index] * c_funcs[i]
        return yp
    else:  # error occured
        return None

def lagint(x_c,y_c,x,nval):
    yi = []
    count = len(x_c)
    x_values = np.zeros(count)
    c_array_type = c_double * count
    x_values = c_array_type()
    for i in range(0, count):
        x_values[i] = x_c[i]
    for xi in x:
        yi.append(lagrange(xi,nval,(x_values),y_c))
    #return lagrange(x,nval,(x_values),y_c)
    return yi

def CiddorHillGroup(wavelength_in_microns, p_in_hPa, t_in_C, rh, xc):
    # Formula based on Ciddor, Appl. Opt. 35, 1566 (1996) and
    # Ciddor and Hill, Appl. Opt.38, 1663 (1999)
    # 
    # wavelength_in_microns - wavelength in microns
    # p_in_hPa - ambient pressure in hPa
    # t_in_C - temperature in °C
    # rh - relative humidity in %r.h.
    # xc - CO2 contents in ppm
    #
    # svp_over_water:
    #   True: saturation vapor pressure over water
    #   False: saturation vapor pressure over ice
    sigma = 1 / wavelength_in_microns
    #sigma = sigma_in_invmicrons
    #pressure in Pa
    p = p_in_hPa * 100    
    # Temperatur in K
    tk = t_in_C + 273.15
    # Temperatur in °C
    t = t_in_C
    if t > 0:
        # saturation vapor pressure water in Pa: svp
        AA = 0.000012378847 # K^(-2)
        BB = -0.019121316 # 1/K
        CC = 33.93711047
        DD = -6343.1645 # K
        svp = np.exp(AA * tk * tk + BB * tk + CC + DD / tk)
    else:
        # saturation vapor pressure over ice (Ciddor, 1996, Eq (13))
        exp_ice = -2633.5/tk + 12.537
        svp = pow(10,exp_ice)

    # 'enhancement' factor
    alpha = 1.00062
    beta = 0.0000000314 # /Pa
    gamma = 0.00000056 # °C^-2

    f = alpha + beta * p + gamma * t * t

    # relative humidity
    h = rh / 100

    # water vapor fraction xw 
    xw = f * h * svp / p

    # index of refraction standard atmosphere
    k0 = 238.0185 # \mu m^-2
    k1 = 5792105 # \mu m^-2
    k2 = 57.362 # \mu m^-2
    k3 = 167917 # \mu m^-2

    nas = (k1 / (k0 - sigma**2) + k3 / (k2 - sigma**2)) * 0.00000001
    naxs = nas * (1 + 0.000000534 * (xc - 450))

    # index of refraction water vapor:
    cf = 1.022
    w0 = 295.235 # \mu m^-2
    w1 = 2.6422 # \mu m^-2
    w2 = -0.03238 # \mu m^-4
    w3 = 0.004028 # \mu m^-6

    nws = cf * (w0 + w1 * sigma**2 + w2 * sigma**4 + w3 * sigma**6) * 0.00000001

    # Ma - molecular mass dry air
    Ma = 0.001 * (28.9635 + 0.000012011 * (xc - 400))

    # Za - compressibility standard dry air
    a0 = 0.00000158123 # K/Pa
    a1 = -0.000000029331 # /Pa
    a2 = 0.00000000011043 # /K/Pa
    b0 = 0.000005707 # K/Pa
    b1 = -0.00000002051 # /Pa
    c0 = 0.00019898 # K/Pa
    c1 = -0.000002376 # /Pa
    d = 0.0000000000183 # K^2/Pa
    e = -0.00000000765 # K^2/Pa^2

    tkref = 288.15 # K
    tref = tkref - 273.15
    pref = 101325 # Pa
    xwref = 0 #

    Za = 1 - pref / tkref * (a0 + a1 * tref + a2 * tref**2 + (b0 + b1 * tref) \
                             * xwref + (c0 + c1 * tref) * xwref**2)
    Za = Za + (pref / tkref)**2 * (d + e * xwref**2)

    # Zw - compressibility pure water vapor
    tkref = 293.15 # K
    tref = tkref - 273.15
    pref = 1333 # Pa
    xwref = 1 #
    Zw = 1 - pref / tkref * (a0 + a1 * tref + a2 * tref**2 + (b0 + b1 * tref) \
                             * xwref + (c0 + c1 * tref) * xwref**2)
    Zw = Zw + (pref / tkref)**2 * (d + e * xwref**2)

    # rhoaxs - density of standard air
    # Mw - molar mass pure water vapor
    Mw = 0.018015 # kg/mol
    # gas constant:
    R = 8.31451 # J/mol/K
    xwref = 0
    pref = 101325
    tkref = 288.15
    rhoaxs = (pref * Ma / (Za * R * tkref)) * (1 - xwref * (1 - Mw / Ma))

    # rhows density of standard water vapor
    xwref = 1
    pref = 1333
    tkref = 293.15
    rhows = (pref * Ma / (Zw * R * tkref)) * (1 - xwref * (1 - Mw / Ma))

    # actual compressibility (t,p, relative humidity, ...)
    Z = 1 - p / tk * (a0 + a1 * t + a2 * t**2 + (b0 + b1 * t) * xw + \
                      (c0 + c1 * t) * xw**2)
    Z = Z + (p / tk)**2 * (d + e * xw**2)

    # density of dry air
    rhoa = p * Ma * (1 - xw) / Z / R / tk

    # density of water vapor
    rhow = p * Mw * xw / Z / R / tk

    # phase index of refraction Ciddor, 1996
    nprop = rhoa / rhoaxs * naxs + rhow / rhows * nws

    #beginning of group refractive index calculation according
    # to Ciddor and Hill, 1999
    nph = nprop + 1

    #Derivative index of refraction standard atmosphere (modified (B2))
    dnas = 2 * sigma * ((k1 / ((k0 - sigma**2) * (k0 - sigma**2))+ \
                          k3 / ((k2 - sigma**2) * (k2 - sigma**2)))) * 0.00000001 
    dnaxs = dnas * (1 + 0.000000534 * (xc - 450))

    #derivative index of refraction of water vapor
    dnws = 2 * cf * (w1 * sigma + 2 * w2 * sigma**3 + 3 * w3 * sigma**5) * 0.00000001

    #Combination using Lorentz-Lorenz relation (Eq (7) of 
    #Ciddor und Hill, 1999)
    prefac  = sigma * (nph * nph + 2) * (nph * nph + 2) / nph
    naxs += 1
    nws += 1
    SumAXS = ((naxs / (naxs * naxs + 2)) / (naxs * naxs + 2)) * rhoa / rhoaxs * dnaxs
    SumW = ((nws / (nws * nws + 2)) / (nws * nws + 2)) * rhow / rhows * dnws
    ng = nph + prefac * (SumAXS + SumW)
    return ng 

# delay in d distance between the ground target and reference point
def delay(wavelength, p_in_hPa_ini , t_in_C_ini, rh, xc, n_steps):
    d = 2720.801
    h = (432.886 - 405.713)
    d_step = d / n_steps
    h_step = h / n_steps
    del_step = 0
    for i in range(n_steps):
        #delta_d = d_step * i
        delta_h = h_step * i
        H = 8e3 # m
        p_in_hPa = p_in_hPa_ini * np.exp(-delta_h / H)
        t_in_C = t_in_C_ini - 6.72e-3 * delta_h
        n = CiddorHillGroup(wavelength, p_in_hPa, t_in_C, rh, xc)
        del_step += n * d_step
    return del_step

def copy_folds(fold_obs,fold_pro,*args):
    if not os.path.exists(fold_pro):
        os.mkdir(fold_pro)
    if len(args) == 0:
        fold_name = fold_obs + '{}/'.format(dt.utcnow().strftime('%Y'))
        fold_proc = fold_pro + '{}/'.format(dt.utcnow().strftime('%Y'))
        fold00 = dt.utcnow().strftime('%Y%m%d')
    else:
        fold_obs_date = args[0]
        fold_obs_year = fold_obs_date[0:4]
        fold_name = fold_obs + '{}/'.format(fold_obs_year)
        fold_proc = fold_pro + '{}/'.format(fold_obs_year)
        fold00 = fold_obs_date
    if not os.path.exists(fold_proc):
        os.mkdir(fold_proc)
    if not os.path.exists(fold_proc + fold00 + '/'):
        os.mkdir(fold_proc + fold00 + '/')
    folds = os.listdir(fold_name + fold00)
    for fold in folds:        
        files = os.listdir(fold_name + fold00 + '/'+fold)        
        if len(files) >= 6:
            for file in files:                
                if file.endswith(".c00"):
                    if not os.path.exists(fold_proc + fold00 + '/'+fold):
                        os.mkdir(fold_proc + fold00 + '/'+fold)
                    sta_name = file[0:-4]
                    print(sta_name)
                    if not os.path.exists(fold_proc + fold00 + '/'+fold+'/'+sta_name):
                        os.mkdir(fold_proc + fold00 + '/'+fold+'/'+sta_name)
                    for file in files:           
                        if file.startswith(sta_name):
                            if os.path.isfile(fold_name + fold00 + '/'+fold+'/'+file):                                
                                shutil.copy(fold_name + fold00 + '/'+fold+'/'+file,fold_proc + fold00 + '/'+fold+'/'+sta_name+'/')  

def open_pre_data(file):
    sod = []
    tof = []    
    f = open(file, 'r')
    lines = f.readlines()[1:]
    for line in lines:
        s = line.split()
        hour = np.double(s[0])
        minute = np.double(s[1])
        second = np.double(s[2])
        sod.append(hour*3600+minute*60+second)
        tof.append(np.double(s[9]))
    vel = np.zeros(len(tof))
    vel[1::] = ((np.array(tof))[1::] - (np.array(tof))[0:-1])/2*CMPS
    f.close()
    return sod, tof, vel

def open_c00(file):
    f = open(file, 'r')
    array = np.genfromtxt(f, skip_header=2)
    headers = ['launch_time_integer', 'launch_time_fraction']
    if len(array) > 2 :
        launch_time = pd.DataFrame(data=array, columns=headers)
    else:
        launch_time = pd.DataFrame(data=[], columns=headers)
    emission_time = launch_time.launch_time_integer+launch_time.launch_time_fraction
    f.close()
    return emission_time

def interp_pre_data(out_file, pre_sod, pre_tof, pre_vel, launch_time):
    output_f = open(out_file,'w')
    #for emi in launch_time:                             
    #pre_TOF,pre_VEL = lagint(pre_sod,[pre_tof,pre_vel],launch_time,10)
    pre_TOF_VEL = lagint(pre_sod,[pre_tof,pre_vel],launch_time,10)
    for i in range(len(launch_time)):
        # first column of pre_TOF_VEL
        return_time = pre_TOF_VEL[i][0] + launch_time[i]
        #return_time = pre_TOF + launch_time
        return_time_int = np.floor(return_time)
        return_time_fra = return_time - return_time_int  
        output_f.write("%d %.12f %.12f %.4f\n" % \
                    (return_time_int,return_time_fra,launch_time[i],pre_TOF_VEL[i][1]))
    output_f.close()

def interp_pre_gt2(out_file, launch_time,pth_file):
    output_f = open(out_file,'w')
    pth = loadtxt(pth_file)
    baroPressure = np.double(pth[1])
    temperature = np.double(pth[2])
    RH = np.double(pth[3])
    if (baroPressure == 0) & (temperature == 0) & (RH == 0):
        # default PTH
        baroPressure, temperature, RH = 960, 25, 80
    for emi in launch_time:   
        wavelength = 1.064        
        xc = 380               
        pre_VEL = 0
        n_steps = 20
        pre_tof = 2 * delay(wavelength, baroPressure, temperature, RH, xc, n_steps) / CMPS
        return_time = pre_tof + emi
        return_time_int = np.floor(return_time)
        return_time_fra = return_time - return_time_int  
        output_f.write("%d %.12f %.12f %.4f\n" % \
                        (return_time_int,return_time_fra,emi,pre_VEL))  
    output_f.close()

def interp_pre_gt2_test(out_file, launch_time, pth_file):
    output_f = open(out_file,'w')
    pth = loadtxt(pth_file)
    baroPressure = np.double(pth[1])
    temperature = np.double(pth[2])
    RH = np.double(pth[3])
    if (baroPressure == 0) & (temperature == 0) & (RH == 0):
        # default PTH
        baroPressure, temperature, RH = 960, 25, 80
    for emi in launch_time:   
        wavelength = 1.064        
        xc = 380
        ng = CiddorHillGroup(wavelength, baroPressure, temperature, RH, xc)
        d = 1.146 + 0.514
        pre_TOF = 2 * d * ng / CMPS
        pre_VEL=0
        return_time = pre_TOF + emi
        return_time_int = np.floor(return_time)
        return_time_fra = return_time - return_time_int  
        output_f.write("%d %.12f %.12f %.4f\n" % \
                        (return_time_int,return_time_fra,emi,pre_VEL))  
    output_f.close()

def interp_pre_fibre_test(out_file, launch_time, pth_file):
    output_f = open(out_file,'w')
    pth = loadtxt(pth_file)
    baroPressure = np.double(pth[1])
    #temperature = np.double(pth[2])
    #RH = np.double(pth[3])
    for emi in launch_time:   
        wavelength = 1.064        
        #xc = 380
        # PTH in Coude Path
        ng = CiddorHillGroup(wavelength, baroPressure, temperature=25, RH=50, xc=380)
        d = 50
        pre_TOF = d * ng / CMPS
        pre_VEL=0
        return_time = pre_TOF + emi
        return_time_int = np.floor(return_time)
        return_time_fra = return_time - return_time_int  
        output_f.write("%d %.12f %.12f %.4f\n" % \
                        (return_time_int,return_time_fra,emi,pre_VEL))  
    output_f.close()

#pre_sod, pre_tof, pre_vel = open_pre_data('/run/media/zhang/DATA/pred/2021/20211125/20211125T100000.ci5')

def match(pre,obs,out):    
    #bytes.decode('utf-8')
    libres.getres(pre.encode('utf-8'),obs.encode('utf-8'),out.encode('utf-8'))

def cal_delta(args,x,y):
    delta_y = []
    y_fit = []
    for xi in x:
        yi = 0
        for i in range(len(args)):
            yi = yi + args[i] * (xi ** (len(args)-(i+1)))
        y_fit.append(yi)
    delta_y = np.array(y) - np.array(y_fit)

    delta = np.std(delta_y)
    return np.array(y_fit),np.array(delta)

def res_filter(rec,rec_filter):
    f = np.loadtxt(rec)
    if len(f) > 100:
        time = f[:,0]
        residual = f[:,1]
        vel = f[:,2]
        tof = f[:,3]
        oo = 5e-9 # threshold: 5 nanoseconds
        w = 0
        w2 = []
        i = 0   # slope
        for j in np.arange(-500e-9,500e-9,0.4e-9): # intercept
            pos0 = []
            y = i * (time-np.min(time)) + j
            pos = np.array(np.where((residual > y) & (residual < (y+oo))))
            if pos.size > 0:            
                pos0 = np.array(pos[0])
                w1 = len(pos0)
                if w < w1:
                    w = w1
                    w2 = pos0 # data position in a rectangle
        if (w > (2e-9*(len(time)-w)/(np.max(residual)-np.min(residual)))*2):
            residual2 = np.array(residual)[w2] #seconds        
            time2 = np.array(time)[w2]
            vel2 = np.array(vel)[w2]
            tof2 = np.array(tof)[w2]         
            n = 10
            for k in range(n):
                residual20 = []
                time20 = []
                vel20 = []
                tof20 = []
                arg1,arg2,arg3,arg4 = polyfit(time2,residual2,3)
                y_fit,delta = cal_delta(np.array([arg1,arg2,arg3,arg4]),time2,residual2)
                for l,res in enumerate(residual2):                    
                    if abs(res-y_fit[l]) < 2.5 * delta:
                        residual20.append(res)
                        time20.append(time2[l])
                        vel20.append(vel2[l])
                        tof20.append(tof2[l])
                if len(time2) == len(time20):
                    break
                else:
                    time2 = time20
                    residual2 = residual20
                    vel2 = vel20
                    tof2 = tof20
            #print(residual2)
            time3 = np.array(time2)
            residual3 = np.array(residual2)
            vel3 = np.array(vel2)
            tof3 = np.array(tof2)

            if len(time3)>20:
                output_f = open(rec_filter,'w')
                for l,tim in enumerate(time3):
                    output_f.write("%.12f %.12f %.4f %.12f\n" % \
                                (tim,residual3[l],vel3[l],tof3[l]))
                output_f.close()

def res_filter_gt2(rec,rec_filter):
    f = np.loadtxt(rec)
    if len(f) > 100:
        time = f[:,0]
        residual = f[:,1]
        vel = f[:,2]
        tof = f[:,3]
        oo = 2e-9 # threshold: 2 nanoseconds
        w = 0
        w2 = []
        for j in np.arange(225e-9,248e-9,0.4e-9): # intercept
            pos0 = []
            y = j
            pos = np.array(np.where((residual > y) & (residual < (y+oo))))
            if pos.size > 0:            
                pos0 = np.array(pos[0])
                w1 = len(pos0)
                if w < w1:
                    w = w1
                    w2 = pos0 # data position in a rectangle
        if (w > (2e-9*(len(time)-w)/(np.max(residual)-np.min(residual)))*2):
            residual2 = np.array(residual)[w2] #seconds        
            time2 = np.array(time)[w2]
            vel2 = np.array(vel)[w2]
            tof2 = np.array(tof)[w2]            
            n = 10
            for k in range(n):
                residual20 = []
                time20 = []
                vel20 = []
                tof20 = []
                arg1,arg2 = polyfit(time2,residual2,1)
                y_fit,delta = cal_delta(np.array([arg1,arg2]),time2,residual2)
                for l,res in enumerate(residual2):                    
                    if abs(res-y_fit[l]) < 2.5 * delta:
                        residual20.append(res)
                        time20.append(time2[l])
                        vel20.append(vel2[l])
                        tof20.append(tof2[l])
                if len(time2) == len(time20):
                    break
                else:
                    time2 = time20
                    residual2 = residual20
                    vel2 = vel20
                    tof2 = tof20
            #print(residual2)
            time3 = np.array(time2)
            residual3 = np.array(residual2)
            vel3 = np.array(vel2)
            tof3 = np.array(tof2)

            if len(time3)>20:
                output_f = open(rec_filter,'w')
                for l,tim in enumerate(time3):
                    output_f.write("%.12f %.12f %.4f %.12f\n" % \
                                (tim,residual3[l],vel3[l],tof3[l]))
                output_f.close()

def cal_return_rate(eff):
    f = np.loadtxt(eff)
    if len(f)>10:
        time = f[:,0]
        return len(time)/(np.max(time) - np.min(time))
    else:
        return 0

def sec2hms(sec):
    h = int(sec/3600)
    m = int((sec-h*3600)/60)
    s = sec-h*3600-m*60
    return h,m,s

def time_infor(rec):
    try:
        f = np.loadtxt(rec,encoding='utf-8',skiprows=2)    
        if len(f)>10:
            time = f[:,0] + f[:,1]
            h0,m0,s0 = sec2hms(np.min(time))
            h1,m1,s1 = sec2hms(np.max(time))
            duration = np.max(time) - np.min(time)
            return h0,m0,s0,h1,m1,s1,duration
        else:
            return 0,0,0,0,0,0,0
    except:
        return 0,0,0,0,0,0,0

