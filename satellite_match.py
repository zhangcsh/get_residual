from satellite import *
# raw data
fold_obs = 'G:/data/'
# processed data
fold_pro = 'G:/process/'
# pre data file
fold_pre = 'G:/pred/'
# report file
fold_rep = 'G:/report/'
if not os.path.exists(fold_rep):
    os.mkdir(fold_rep)

# load command from terminal
if len(sys.argv) > 1:
    fold_obs_date = sys.argv[1]
else:
    fold_obs_date = dt.now().strftime('%Y%m%d')
fold_obs_year = fold_obs_date[0:4]

copy_folds(fold_obs,fold_pro,fold_obs_date)
fold_obs = fold_pro
fold_obs_files = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date)

for file_fold in fold_obs_files:
    name_split = file_fold.split('.')   
    if len(name_split) == 2:
        ab = name_split[1]
    elif len(name_split) > 2:
        ab = name_split[1] + '.' + name_split[2]
    
    if ((ab != 'gt2') and (ab != 'gt2.test')):
        files = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold)
        for file in files:
            #if file.startswith('20191025T133153.la2'):
            try:
                print(file)
                file_obs = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file)                
                pre = fold_pre + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold
                #f = open(f,'r')
                pre_sod, pre_tof, pre_vel = open_pre_data(pre)
                for f_obs in file_obs:
                    out_file = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' + file + '/back_time'
                    if f_obs.endswith('.c00'):
                        launch_time = open_c00(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs)
                        interp_pre_data(out_file, pre_sod, pre_tof, pre_vel, launch_time)

                    elif f_obs.endswith('.c01'):                                                                            
                        obs01 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out01 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c1'
                        match(out_file,obs01,out01)
                    elif f_obs.endswith('.c02'):
                        obs02 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out02 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c2'
                        match(out_file,obs02,out02)
                    elif f_obs.endswith('.c03'):
                        obs03 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out03 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c3'
                        match(out_file,obs03,out03)
                    elif f_obs.endswith('.c04'):
                        obs04 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out04 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c4'
                        match(out_file,obs04,out04)
            except:continue 
            
        for file in files:
            try:
                #print(file)                                    
                file_obs = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file)
                for f_obs in file_obs:
                    if f_obs.endswith('.c1'):
                        rec1 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e1'
                        res_filter(rec1,output_f)
                    if f_obs.endswith('.c2'):
                        rec2 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e2'
                        res_filter(rec2,output_f)
                    if f_obs.endswith('.c3'):
                        rec3 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e3'
                        res_filter(rec3,output_f)
                    if f_obs.endswith('.c4'):
                        rec4 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e4'
                        res_filter(rec4,output_f)
            except:
                continue
    elif ab == 'gt2':
        files = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold)
        for file in files:
            try:
                #print(file)                                    
                file_obs = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file)                               
        
                for f_obs in file_obs:
                    out_file = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' + file + '/back_time'
                    if f_obs.endswith('.pth'):
                        pth_file = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                    if f_obs.endswith('.c00'):
                        launch_time = open_c00(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs)
                interp_pre_gt2(out_file, launch_time, pth_file)                                     

                for f_obs in file_obs:                                        
                    if f_obs.endswith('.c01'):                                                                            
                        obs01 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out01 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c1'
                        match(out_file,obs01,out01)
                    elif f_obs.endswith('.c02'):
                        obs02 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out02 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c2'
                        match(out_file,obs02,out02)
                    elif f_obs.endswith('.c03'):
                        obs03 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out03 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c3'
                        match(out_file,obs03,out03)
                    elif f_obs.endswith('.c04'):
                        obs04 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out04 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c4'
                        match(out_file,obs04,out04)
            except:continue
        for file in files:
            try:
                #print(file)                                    
                file_obs = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file)                                            
                for f_obs in file_obs:
                    if f_obs.endswith('.c1'):
                        rec1 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e1'
                        res_filter_gt2(rec1,output_f)
                    if f_obs.endswith('.c2'):
                        rec2 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e2'
                        res_filter_gt2(rec2,output_f)     
                    if f_obs.endswith('.c3'):
                        rec3 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e3'
                        res_filter_gt2(rec3,output_f)
                    if f_obs.endswith('.c4'):
                        rec4 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e4'
                        res_filter_gt2(rec4,output_f)
            except:
                continue
    elif ab == 'gt2.test':
        files = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold)
        for file in files:
            try:
                #print(file)                                    
                file_obs = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file)                                       
                for f_obs in file_obs:
                    out_file = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' + file + '/back_time'
                    if f_obs.endswith('.pth'):
                        pth_file = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                    if f_obs.endswith('.c00'):
                        launch_time = open_c00(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs)
                interp_pre_gt2_test(out_file, launch_time,pth_file)                                     
                for f_obs in file_obs:                                        
                    if f_obs.endswith('.c01'):                                                                            
                        obs01 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out01 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c1'
                        match(out_file,obs01,out01)
                    elif f_obs.endswith('.c02'):
                        obs02 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out02 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c2'
                        match(out_file,obs02,out02)
                    elif f_obs.endswith('.c03'):
                        obs03 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out03 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c3'
                        match(out_file,obs03,out03)
                    elif f_obs.endswith('.c04'):
                        obs04 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        out04 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.c4'
                        match(out_file,obs04,out04)
            except:continue            
        
        for file in files:
            try:
                #print(file)                                    
                file_obs = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file)                                            
                for f_obs in file_obs:
                    if f_obs.endswith('.c1'):
                        rec1 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e1'
                        res_filter_gt2(rec1,output_f)
                    if f_obs.endswith('.c2'):
                        rec2 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e2'
                        res_filter_gt2(rec2,output_f)     
                    if f_obs.endswith('.c3'):
                        rec3 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e3'
                        res_filter_gt2(rec3,output_f)
                    if f_obs.endswith('.c4'):
                        rec4 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        output_f = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + 'rec.e4'
                        res_filter_gt2(rec4,output_f)     
            except:
                continue     

# output laser ranging report file            
output_r = open(fold_rep + '/lr_report_'+fold_obs_date, 'w')
print('Laser ranging report for '+fold_obs_date)
output_r.write('%s激光测距结果统计\n' % fold_obs_date)
output_r.write('观测各目标时长及各通道回波率\n')
output_r.write("---------------------------\n")
st = 0
gt = 0
n_eff = 0      
for file_fold in fold_obs_files:
    name_split = file_fold.split('.')       
    if len(name_split) == 2:
        ab = name_split[1]
    elif len(name_split) > 2:
        ab = name_split[1] + '.' + name_split[2]                    
    if ((ab != 'gt2') and (ab != 'gt2.test')):
        st += 1
    if ab == 'gt2':
        gt += 1
    if ab != 'gt2.test':
        files = os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold)
        for file in files:
            rr1, rr2, rr3, rr4 = 0, 0, 0, 0
            try:
                for f_obs in os.listdir(fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file):
                    if f_obs.endswith('.e1'):
                        rec1 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs          
                        rr1 = cal_return_rate(rec1)
                    elif f_obs.endswith('.e2'):
                        rec2 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        rr2 = cal_return_rate(rec2)
                    elif f_obs.endswith('.e3'):
                        rec3 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        rr3 = cal_return_rate(rec3)
                    elif f_obs.endswith('.e4'):
                        rec4 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        rr4 = cal_return_rate(rec4)
                    elif f_obs.endswith('.c00'):
                        rec0 = fold_obs + fold_obs_year +'/'+ fold_obs_date + '/' +file_fold + '/' +file + '/' + f_obs
                        h0,m0,s0,h1,m1,s1,duration = time_infor(rec0)
                if rr1 != 0 or rr2 != 0 or rr3 != 0 or rr4 != 0:
                    n_eff += 1
                output_r.write("目标：%s 开始时刻：%d:%d:%.2f——结束时刻：%d:%d:%.2f 持续时长：%.2f秒\n" % (ab,h0,m0,s0,h1,m1,s1,duration))
                output_r.write("    通道1：%f 通道2：%f 通道3：%f 通道4：%f\n" % (rr1,rr2,rr3,rr4))
                output_r.write("\n")
            except:
                continue
output_r.write("测量空间目标数量：%d\n" % st)
output_r.write("测量3km地靶次数：%d\n" % gt)
output_r.write("有效测量目标数量：%d\n" % n_eff)
output_r.write("------------------------------------------------\n")
output_r.write('说明：时间（UTC）为0表示主波信号异常或无主波信号\n')
output_r.write('      回波率为0表示某个通道工作异常或无有效回波数据\n')
output_r.close()



                        
                                            
